<?php
/**
 * @file
 * phoenix_contact.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function phoenix_contact_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'contact';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_compro_component';
  $view->human_name = 'Contact';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Component: Sub Category */
  $handler->display->display_options['fields']['field_contact_sub_category']['id'] = 'field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['table'] = 'field_data_field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['field'] = 'field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['label'] = '';
  $handler->display->display_options['fields']['field_contact_sub_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_contact_sub_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_sub_category']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Global: Fieldset */
  $handler->display->display_options['fields']['fieldset']['id'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset']['table'] = 'views';
  $handler->display->display_options['fields']['fieldset']['field'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset']['children'] = array(
    0 => 'title',
    1 => 'field_contact_title',
  );
  $handler->display->display_options['fields']['fieldset']['fieldset']['type'] = 'div';
  $handler->display->display_options['fields']['fieldset']['fieldset']['classes'] = 'left';
  /* Field: Component: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_compro_component';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Component: Job Title */
  $handler->display->display_options['fields']['field_contact_title']['id'] = 'field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['table'] = 'field_data_field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['field'] = 'field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['label'] = '';
  $handler->display->display_options['fields']['field_contact_title']['element_label_colon'] = FALSE;
  /* Field: Global: Fieldset */
  $handler->display->display_options['fields']['fieldset_1']['id'] = 'fieldset_1';
  $handler->display->display_options['fields']['fieldset_1']['table'] = 'views';
  $handler->display->display_options['fields']['fieldset_1']['field'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset_1']['children'] = array(
    0 => 'field_contact_office_phone',
    1 => 'field_contact_mobile_phone',
    2 => 'field_contact_e_mail',
  );
  $handler->display->display_options['fields']['fieldset_1']['fieldset']['type'] = 'div';
  $handler->display->display_options['fields']['fieldset_1']['fieldset']['classes'] = 'right';
  /* Field: Component: Office Phone */
  $handler->display->display_options['fields']['field_contact_office_phone']['id'] = 'field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['table'] = 'field_data_field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['field'] = 'field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['label'] = 'Office';
  $handler->display->display_options['fields']['field_contact_office_phone']['hide_empty'] = TRUE;
  /* Field: Component: Mobile Phone */
  $handler->display->display_options['fields']['field_contact_mobile_phone']['id'] = 'field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['table'] = 'field_data_field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['field'] = 'field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['label'] = 'Mobile';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['hide_empty'] = TRUE;
  /* Field: Component: E-mail */
  $handler->display->display_options['fields']['field_contact_e_mail']['id'] = 'field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['table'] = 'field_data_field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['field'] = 'field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['label'] = 'Email';
  $handler->display->display_options['fields']['field_contact_e_mail']['hide_empty'] = TRUE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  /* Filter criterion: Component: Category (field_contact_category) */
  $handler->display->display_options['filters']['field_contact_category_tid']['id'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['table'] = 'field_data_field_contact_category';
  $handler->display->display_options['filters']['field_contact_category_tid']['field'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['value'] = array(
    7 => '7',
  );
  $handler->display->display_options['filters']['field_contact_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_contact_category_tid']['vocabulary'] = 'contact_category';

  /* Display: General Inquiry Contacts */
  $handler = $view->new_display('block', 'General Inquiry Contacts', 'general_inquiry_contacts_block');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  /* Filter criterion: Component: Category (field_contact_category) */
  $handler->display->display_options['filters']['field_contact_category_tid']['id'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['table'] = 'field_data_field_contact_category';
  $handler->display->display_options['filters']['field_contact_category_tid']['field'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['value'] = array(
    55 => '55',
  );
  $handler->display->display_options['filters']['field_contact_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_contact_category_tid']['vocabulary'] = 'contact_category';
  $handler->display->display_options['block_description'] = 'General Inquiry Contacts';

  /* Display: Sales Contacts */
  $handler = $view->new_display('block', 'Sales Contacts', 'sales_contacts_block');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_contact_sub_category',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Component: Sub Category */
  $handler->display->display_options['fields']['field_contact_sub_category']['id'] = 'field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['table'] = 'field_data_field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['field'] = 'field_contact_sub_category';
  $handler->display->display_options['fields']['field_contact_sub_category']['label'] = '';
  $handler->display->display_options['fields']['field_contact_sub_category']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_contact_sub_category']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_contact_sub_category']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Global: Fieldset */
  $handler->display->display_options['fields']['fieldset']['id'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset']['table'] = 'views';
  $handler->display->display_options['fields']['fieldset']['field'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset']['children'] = array(
    0 => 'field_contact_region',
  );
  $handler->display->display_options['fields']['fieldset']['fieldset']['type'] = 'div';
  $handler->display->display_options['fields']['fieldset']['fieldset']['classes'] = 'left';
  /* Field: Component: Region */
  $handler->display->display_options['fields']['field_contact_region']['id'] = 'field_contact_region';
  $handler->display->display_options['fields']['field_contact_region']['table'] = 'field_data_field_contact_region';
  $handler->display->display_options['fields']['field_contact_region']['field'] = 'field_contact_region';
  $handler->display->display_options['fields']['field_contact_region']['label'] = '';
  $handler->display->display_options['fields']['field_contact_region']['element_label_colon'] = FALSE;
  /* Field: Global: Fieldset */
  $handler->display->display_options['fields']['fieldset_1']['id'] = 'fieldset_1';
  $handler->display->display_options['fields']['fieldset_1']['table'] = 'views';
  $handler->display->display_options['fields']['fieldset_1']['field'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset_1']['children'] = array(
    0 => 'title',
    1 => 'field_contact_title',
  );
  $handler->display->display_options['fields']['fieldset_1']['fieldset']['type'] = 'div';
  /* Field: Component: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'eck_compro_component';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['element_label_colon'] = FALSE;
  /* Field: Component: Job Title */
  $handler->display->display_options['fields']['field_contact_title']['id'] = 'field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['table'] = 'field_data_field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['field'] = 'field_contact_title';
  $handler->display->display_options['fields']['field_contact_title']['label'] = '';
  $handler->display->display_options['fields']['field_contact_title']['element_label_colon'] = FALSE;
  /* Field: Global: Fieldset */
  $handler->display->display_options['fields']['fieldset_2']['id'] = 'fieldset_2';
  $handler->display->display_options['fields']['fieldset_2']['table'] = 'views';
  $handler->display->display_options['fields']['fieldset_2']['field'] = 'fieldset';
  $handler->display->display_options['fields']['fieldset_2']['children'] = array(
    0 => 'field_contact_office_phone',
    1 => 'field_contact_mobile_phone',
    2 => 'field_contact_e_mail',
  );
  $handler->display->display_options['fields']['fieldset_2']['fieldset']['type'] = 'div';
  /* Field: Component: Office Phone */
  $handler->display->display_options['fields']['field_contact_office_phone']['id'] = 'field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['table'] = 'field_data_field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['field'] = 'field_contact_office_phone';
  $handler->display->display_options['fields']['field_contact_office_phone']['label'] = 'Office';
  $handler->display->display_options['fields']['field_contact_office_phone']['hide_empty'] = TRUE;
  /* Field: Component: Mobile Phone */
  $handler->display->display_options['fields']['field_contact_mobile_phone']['id'] = 'field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['table'] = 'field_data_field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['field'] = 'field_contact_mobile_phone';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['label'] = 'Mobile';
  $handler->display->display_options['fields']['field_contact_mobile_phone']['hide_empty'] = TRUE;
  /* Field: Component: E-mail */
  $handler->display->display_options['fields']['field_contact_e_mail']['id'] = 'field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['table'] = 'field_data_field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['field'] = 'field_contact_e_mail';
  $handler->display->display_options['fields']['field_contact_e_mail']['label'] = 'Email';
  $handler->display->display_options['fields']['field_contact_e_mail']['hide_empty'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  /* Filter criterion: Component: Category (field_contact_category) */
  $handler->display->display_options['filters']['field_contact_category_tid']['id'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['table'] = 'field_data_field_contact_category';
  $handler->display->display_options['filters']['field_contact_category_tid']['field'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['value'] = array(
    56 => '56',
  );
  $handler->display->display_options['filters']['field_contact_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_contact_category_tid']['vocabulary'] = 'contact_category';
  $handler->display->display_options['block_description'] = 'Sales Contacts';

  /* Display: Service & Support Contacts */
  $handler = $view->new_display('block', 'Service & Support Contacts', 'service_support_block');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  /* Filter criterion: Component: Category (field_contact_category) */
  $handler->display->display_options['filters']['field_contact_category_tid']['id'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['table'] = 'field_data_field_contact_category';
  $handler->display->display_options['filters']['field_contact_category_tid']['field'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['value'] = array(
    57 => '57',
  );
  $handler->display->display_options['filters']['field_contact_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_contact_category_tid']['vocabulary'] = 'contact_category';
  $handler->display->display_options['block_description'] = 'Service & Support Contacts';

  /* Display: Parts & Supplies Contacts */
  $handler = $view->new_display('block', 'Parts & Supplies Contacts', 'parts_supplies_block');
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Component: compro_component type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_compro_component';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'contact' => 'contact',
  );
  /* Filter criterion: Component: Category (field_contact_category) */
  $handler->display->display_options['filters']['field_contact_category_tid']['id'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['table'] = 'field_data_field_contact_category';
  $handler->display->display_options['filters']['field_contact_category_tid']['field'] = 'field_contact_category_tid';
  $handler->display->display_options['filters']['field_contact_category_tid']['value'] = array(
    58 => '58',
  );
  $handler->display->display_options['filters']['field_contact_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_contact_category_tid']['vocabulary'] = 'contact_category';
  $handler->display->display_options['block_description'] = 'Parts & Supplies Contacts';
  $export['contact'] = $view;

  return $export;
}
