<?php
/**
 * @file
 * phoenix_contact.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function phoenix_contact_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'key_contacts_directory';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Key Contacts Directory';
  $quicktabs->tabs = array(
    0 => array(
      'bid' => 'views_delta_94e1dec32b9d632ff8408d398c780836',
      'hide_title' => 1,
      'title' => 'General Inquiry',
      'weight' => '-100',
      'type' => 'block',
    ),
    1 => array(
      'bid' => 'views_delta_contact-sales_contacts_block',
      'hide_title' => 1,
      'title' => 'Sales',
      'weight' => '-99',
      'type' => 'block',
    ),
    2 => array(
      'bid' => 'views_delta_contact-service_support_block',
      'hide_title' => 1,
      'title' => 'Service & Support',
      'weight' => '-98',
      'type' => 'block',
    ),
    3 => array(
      'bid' => 'views_delta_contact-parts_supplies_block',
      'hide_title' => 1,
      'title' => 'Parts & Supplies',
      'weight' => '-97',
      'type' => 'block',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('General Inquiry');
  t('Key Contacts Directory');
  t('Parts & Supplies');
  t('Sales');
  t('Service & Support');

  $export['key_contacts_directory'] = $quicktabs;

  return $export;
}
