<?php
/**
 * @file
 * phoenix_contact.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function phoenix_contact_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "quicktabs" && $api == "quicktabs") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function phoenix_contact_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function phoenix_contact_eck_bundle_info() {
  $items = array(
    'compro_component_contact' => array(
      'machine_name' => 'compro_component_contact',
      'entity_type' => 'compro_component',
      'name' => 'contact',
      'label' => 'Contact',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'language' => 0,
        ),
      ),
    ),
  );
  return $items;
}
