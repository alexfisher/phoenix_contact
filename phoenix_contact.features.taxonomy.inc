<?php
/**
 * @file
 * phoenix_contact.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function phoenix_contact_taxonomy_default_vocabularies() {
  return array(
    'contact_category' => array(
      'name' => 'Contact Category',
      'machine_name' => 'contact_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'contact_sub_category' => array(
      'name' => 'Contact Sub Category',
      'machine_name' => 'contact_sub_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
