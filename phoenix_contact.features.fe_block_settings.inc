<?php
/**
 * @file
 * phoenix_contact.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function phoenix_contact_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['quicktabs-key_contacts_directory'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'key_contacts_directory',
    'module' => 'quicktabs',
    'node_types' => array(),
    'pages' => 'page/key-contacts-directory',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'compro_adminimal' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'compro_adminimal',
        'weight' => 0,
      ),
      'compro_theme' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'compro_theme',
        'weight' => -36,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  return $export;
}
